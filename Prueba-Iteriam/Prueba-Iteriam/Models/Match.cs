﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba_Iteriam.Models
{
    public class Match
    {
        public Team LocalTeam { get; set; }
        public int GoalsLocalTeam{ get; set; }
        public Team VisitorTeam { get; set; }
        public int GoalsVisitorTeam { get; set; }
    }
}