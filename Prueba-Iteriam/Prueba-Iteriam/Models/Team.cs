﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba_Iteriam.Models
{
    public class Team
    {
        public Team()
        {
            Statistics = new Statistics();
        }
        public string Name { get; set; }
        public Statistics Statistics { get; set; }

    }
}