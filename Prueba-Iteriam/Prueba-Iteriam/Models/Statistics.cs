﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba_Iteriam.Models
{
    public class Statistics
    {
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Losts { get; set; }
        public int TotalGoalsScored { get; set; }
        public int Points { get { return (Wins * 3) + Draws;} }
    }
}