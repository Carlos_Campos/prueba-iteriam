﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web.Mvc;
using System;

namespace Prueba_Iteriam.Controllers
{
    public class HomeController : Controller
    {
        private const string results = "Manchester United 1 Chelsea 0, Arsenal 1 Manchester United 1, Manchester United 3 Fulham 1, Liverpool 2 Manchester United 1, Swansea 2 Manchester United 4";
        public const string MANCHESTER_UNITED = "Manchester United";
        public static Hashtable teams = new Hashtable();

        public ActionResult Index()
        {
            teams = new Hashtable();
            try
            {
                ViewBag.Title = "Prueba Iteriam";

                List<string> matches = results.Split(',').ToList();
                Regex regex = new Regex(@"([\w|\s]*?)( \d)");

                List<Models.Match> resultsMatches = new List<Models.Match>();

                string localName = null;
                string visitorName = null;
                foreach (var fullMatch in matches)
                {
                    Models.Match match = new Models.Match();
                    var datas = regex.Matches(fullMatch);
                    localName = datas[0].Groups[1].Value.Trim();
                    visitorName = datas[1].Groups[1].Value.Trim();

                    //LocalTeam
                    match.LocalTeam = new Models.Team();
                    match.LocalTeam.Name = localName;
                    match.GoalsLocalTeam = int.Parse(datas[0].Groups[2].Value);
                    //VisitorTeam
                    match.VisitorTeam = new Models.Team();
                    match.VisitorTeam.Name = visitorName;
                    match.GoalsVisitorTeam = int.Parse(datas[1].Groups[2].Value);

                    //Add Model
                    resultsMatches.Add(match);

                    //Add team in list if not exists
                    if (!teams.ContainsKey(localName.ToLower()))
                        CreateTeam(localName);

                    if (!teams.ContainsKey(visitorName.ToLower()))
                        CreateTeam(visitorName);

                    // Add Atributes
                    if (match.GoalsLocalTeam > match.GoalsVisitorTeam)
                    {
                        (teams[localName.ToLower()] as Models.Team).Statistics.Wins++;
                        (teams[visitorName.ToLower()] as Models.Team).Statistics.Losts++;
                    }
                    else if (match.GoalsLocalTeam < match.GoalsVisitorTeam)
                    {
                        (teams[localName.ToLower()] as Models.Team).Statistics.Losts++;
                        (teams[visitorName.ToLower()] as Models.Team).Statistics.Wins++;
                    }
                    else
                    {
                        (teams[localName.ToLower()] as Models.Team).Statistics.Draws++;
                        (teams[visitorName.ToLower()] as Models.Team).Statistics.Draws++;
                    }

                    (teams[localName.ToLower()] as Models.Team).Statistics.TotalGoalsScored += match.GoalsLocalTeam ;
                    (teams[visitorName.ToLower()] as Models.Team).Statistics.TotalGoalsScored += match.GoalsVisitorTeam ;
                }

                ViewBag.manchester_united = teams[MANCHESTER_UNITED.ToLower()];
                ViewBag.fullTeams = teams.Values.Cast<Models.Team>().OrderByDescending(p => p.Statistics.Points);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return View();
        }


        // Private Methods

        private void CreateTeam(string team)
        {
            teams[team.ToLower()] = new Models.Team();
            (teams[team.ToLower()] as Models.Team).Name = team;
        } 
    }
}
