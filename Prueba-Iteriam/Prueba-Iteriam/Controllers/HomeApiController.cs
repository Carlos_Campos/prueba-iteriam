﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Prueba_Iteriam.Controllers
{
    public class HomeApiController : ApiController
    {


        [HttpGet]
        [Route("ApiHome/ExportJSON/{type:int}/")]
        public string ExportJSON(int type)
        {
            string jsonString = null;
            switch (type)
            {
                case 0: // Get statistics only manchester_united
                    jsonString = JsonConvert.SerializeObject(HomeController.teams[HomeController.MANCHESTER_UNITED.ToLower()]).ToString();
                    break;
                case 1: // Get statistics all teams
                    jsonString = JsonConvert.SerializeObject(HomeController.teams).ToString();
                    break;
            }
            return jsonString;

        }
    }
}